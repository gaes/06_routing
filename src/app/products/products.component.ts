import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ActivatedRoute, Params }   from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  {

  //ISTANCE
   id :number;

   //CONSTRUCTOR
   constructor(private route: ActivatedRoute) 
   {
      this.id = this.route.snapshot.params['id'];    
   } 

}
